-- First, draw some circles
require('draw_some_circles')

--then, draw the rest of the owl
require('plugins')
require('display')
require('editing')
require('keybindings')
require('lsp')
