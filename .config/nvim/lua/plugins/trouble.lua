require('trouble').setup()

config.map('<Leader>e', ':TroubleToggle lsp_document_diagnostics<CR>', { silent = true })
