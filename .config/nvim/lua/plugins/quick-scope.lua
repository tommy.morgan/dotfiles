-- only show quickscope hints when trying to search forwards/backwards by
-- characters
vim.g.qs_highlight_on_keys = {'f','F','t','T'}
