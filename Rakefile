# frozen_string_literal: true

require 'yaml'

task default: %w[update_submodules symlink package:all]

task install: %w[default post_install]

DEPENDENCIES = YAML.load_file('dependencies.yaml')

IGNORE_LIST = %w[.config .git .gitignore .gitmodules . ..].freeze
SPECIAL_SYMLINKS = %w[.config/nvim .config/enhance-rails-intellisense-in-solargraph .config/git .config/alacritty.yml .config/alacritty].freeze

desc "Update any and all git submodules"
task :update_submodules do
  puts "Updating all submodules..."
  sh "git submodule update --init --recursive"
  puts "Done!"
end

desc "Add symlinks for any missing files"
task :symlink do
  puts "Checking symlinks..."
  wd = Dir.pwd
  home = Dir.home
  files = Dir[".*"] + SPECIAL_SYMLINKS - IGNORE_LIST
  files.each do |file|
    next if File.exist? "#{home}/#{file}"

    dir = File.dirname("#{home}/#{file}")
    unless File.exist? dir
      puts "Creating directory #{dir}..."
      `mkdir -p #{dir}`
    end
    puts "Symlinking #{file}..."
    `ln -s #{wd}/#{file} #{home}/#{file}`
  end
  puts "Done!"
end

namespace :package do
  desc "Do all the packaging"
  task all: %w[brew asdf pip rubygems casks].freeze

  desc "Install any missing asdf plugins"
  task :asdf do
    installed_plugins = `asdf plugin list`.split("\n")
    install_list = DEPENDENCIES['asdf_plugins'] - installed_plugins
    unless install_list.empty?
      puts "#{install_list.size} plugin#{"s" if install_list.size > 1} to install: #{install_list.join(", ")}"
      install_list.each do |package|
        puts "Installing #{package}..."
        sh "asdf plugin add #{package}"
        puts "Done!"
      end
    end
  end

  desc "Install any homebrew packages required"
  task :brew do
    if `which brew` == ""
      puts "Homebrew not detected, installing..."
      sh "/bin/bash -c '$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)'"
      puts "Done!"
    end

    puts "Checking required homebrew packages..."
    installed_packages = `brew list -1`.split("\n")
    install_list = DEPENDENCIES['brew_packages'] - installed_packages
    unless install_list.empty?
      puts "#{install_list.size} package#{"s" if install_list.size > 1} to install: #{install_list.join(", ")}"
      puts "Updating homebrew..."
      sh "brew update"
      puts "Done!"
      install_list.each do |package|
        puts "Installing #{package}..."
        sh "brew install #{package}"
        puts "Done!"
      end
    end
  end

  desc "Install any pip packages required"
  task :pip do
    puts "Checking required pip packages..."
    installed_packages = `pip3 list`.split("\n").map { |pkg| pkg.split[0] }
    install_list = DEPENDENCIES['pip_packages'] - installed_packages
    unless install_list.empty?
      puts "#{install_list.size} package#{"s" if install_list.size > 1} to install: #{install_list.join(", ")}"
      install_list.each do |package|
        puts "Installing #{package}..."
        sh "pip3 install --user #{package}"
        puts "Done!"
      end
    end
  end

  desc "Install any rubygems required"
  task :rubygems do
    puts "Checking required rubygems..."
    installed_packages = `gem list`.split("\n").map { |pkg| pkg.split[0] }
    install_list = DEPENDENCIES['ruby_gems'] - installed_packages
    unless install_list.empty?
      puts "#{install_list.size} package#{"s" if install_list.size > 1} to install: #{install_list.join(", ")}"
      install_list.each do |package|
        puts "Installing #{package}..."
        sh "gem install #{package}"
        puts "Done!"
      end
    end
  end

  desc "Install any casks required"
  task :casks do
    puts "Checking casks..."
    DEPENDENCIES['casks'].each do |cask_name, app_name|
      if File.exist? "/Applications/#{app_name}.app"
        puts "#{app_name} already installed!"
      else
        puts "Installing #{app_name}..."
        sh "brew install #{cask_name}"
        puts "Done!"
      end
    end
  end
end

task :post_install do
  puts "Running post-installation commands..."
  sh "aws configure"
  puts "downloading ssh key..."
  sh "mkdir ~/.ssh"
  sh "aws secretsmanager get-secret-value --secret-id Personal/SshKey | jq -r '.SecretString' > ~/.ssh/id_ed25519"
  sh "chmod 700 ~/.ssh"
  sh "chmod 600 ~/.ssh/**"
  puts "done"
end
